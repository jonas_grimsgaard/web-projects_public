$(document).on("click touchstart", "#sendMail", function(e){
    e.preventDefault(); // prevent the default click action
    var $form = $('#sendMailForm');
    
    var dat = JSON.stringify($form.serializeObject());
    
    $('#pleaseWaitDialog').modal('toggle');
    
    $.get('/sendmail', JSON.parse(dat), function (data) {        
        console.log(data);
        if(data == "sent"){
            $('#pleaseWaitDialog').modal('toggle');
            $('#myModalSuccess').modal('toggle');
        }
        else{
            $('#pleaseWaitDialog').modal('toggle');
            $('.error-message ul').empty();
            data.forEach(function(element) {
                $('.error-message ul').append('<li>' + element.msg + '</li>')
            }, this);
            $('#errorModal').modal('toggle');
        }
        
    });
});

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};