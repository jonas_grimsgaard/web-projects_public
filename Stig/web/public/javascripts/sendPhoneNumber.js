$(document).on("click touchstart", "#sendTLF", function(e){
    e.preventDefault(); // prevent the default click action
    
    var phonenumber = $('#telephoneNumber').val();  
         
    $('#pleaseWaitDialog').modal('toggle');
    
    $.get('/notifyTelephone', {'phone': phonenumber}, function (data) {
        console.log(data);
        if(data == "sent"){
            $('#pleaseWaitDialog').modal('toggle');
            $('#motattTlf').text(phonenumber);
            $('#myModal').modal('toggle');
            console.log("Success");
        }
        else {
            $('#pleaseWaitDialog').modal('toggle');
            $('.error-message ul').empty();
            data.forEach(function(element) {
                $('.error-message ul').append('<li>' + element.msg + '</li>')
            }, this);
            $('#errorModal').modal('toggle');
        }
    });
});

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};