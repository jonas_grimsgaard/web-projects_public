var express = require('express');
var router = express.Router();
var telegram = require('../controller/telegram.js');

router.get('/notifyTelephone', function(req,res){
    req.checkQuery('phone', 'Ugyldig telefonnummer').notEmpty().isMobilePhone('nb-NO');
    
    var errors = req.validationErrors();
    if(errors){
        res.send(errors);
        return;
    }
    var phone = req.query.phone; 
    telegram.log("Hei, du har fått en forespørsel fra dette nummeret: " +  phone);
    res.end("sent");
});

module.exports = router;