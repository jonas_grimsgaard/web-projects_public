var express = require('express');
var router = express.Router();
var telegram = require('../controller/telegram.js');
var email = require('../controller/emailProvider.js');

router.get('/sendmail', function (req,res) {
    
    req.checkQuery('email', 'Ugyldig epost adresse').isEmail();
    req.checkQuery('comments', 'Kommentarfeltet er tomt').notEmpty();
    req.checkQuery('name', 'Vennligst skriv inn et navn').notEmpty();
    
    var errors = req.validationErrors();
    if(errors){
        res.send(errors);
        return;
    }
    
    var val = req.query;
    
    var mailOptions={
        to : "jonasbg+nodemailer@gmail.com",
        from : val.name + '<' + val.email +'>',
        subject : "Melding fra Stig Liabø - nettside",
        text : "Navn: " + val.name + "\n" + "Epost: " + val.email + "\n" + "Beskjed: " + val.comments
    }
    
    email.sendMail(res, mailOptions);
    telegram.log("Det er sendt ut en epost fra: " + val.email);
});

module.exports = router;