var express = require('express');
var router = express.Router();
var telegram = require('../controller/telegram.js');

/* GET home page. */
router.get('/', function(req, res, next) {
  telegram.log("You have a visitor");
  res.render('index', {
        title:'Stig Liabø Maskinstasjon',
        tlf: '911 43 395',
        orgnr: '963 489 870'}
        );
});

module.exports = router;
