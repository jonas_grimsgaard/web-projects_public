# README #

Små NodeJS prosjekter.

Stig\Web er en NodeJS applikasjon som bruker Express, Bootstrap for frontend, JQuery, Jade-lang for HTML syntax. Denne applikasjonen kjører i et DOCKER miljø installert på min NAS og er tilgjengelig på denne adressen:: http://whitedog.no

Pingr er en annen NodeJS applikasjon der man kan legge til IP adresser som så blir pinget ved et gitt intervall. For hvert ping sesjon blir svaret lagret i en MongoDB database. På min NAS er dette satt opp i to DOCKER konteinere, der den ene (NodeJS) er linket til MongoDB konteineren. 

De andre web prosjektene er prosjekter som jeg aldri kom skikkelig igang med.