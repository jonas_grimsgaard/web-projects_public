var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST
    
var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');
var ObjectID = require('mongodb').ObjectId;
var url = 'mongodb://mongo:27017/pingr';

router.use(bodyParser.urlencoded({ extended: true }));
router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}))

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.delete('/', function (req, res) {
    var ip = req.query.ip;
    console.log("Got the following IP to delete: " + ip)
    
     MongoClient.connect(url, function (err, db) {
         var computer = db.collection('computers');
         
         computer.remove({'ip':ip}, function (err, doc) {
             if(err)
                {
                    db.close();
                    return console.error(err);;
                }
            console.log('sucessfully deleted ' + doc + ' documents');
            db.close();
            return res.send("Deleted " + req.query.ip)
         })
     });
})

var updateComputerName = function(ip, dns, db, callback) {
   db.collection('computers').updateOne(
      { "ip" : ip },
      {
        $set: { "dns": dns }
      }, function(err, results) {
      console.log(results);
      callback();
   });
};

router.post('/update', function (req,res){
    console.log(req.query);
    var dns = req.query.newName;
    var ip = req.query.ip;
    
    MongoClient.connect(url, function(err, db){
        if(err){
                db.close();
                return console.error(err);
            }
        
        updateComputerName(ip, dns, db, function(){
            db.close();
        })
    });
    res.send("success");
})

router.post('/', function (req, res) {
    var _ip = req.body.ip;
    var _dns = req.body.dns;
    
    MongoClient.connect(url, function (err, db) {
        var computer = db.collection("computers");
        
        computer.insert({
            ip: _ip,
            dns: _dns,
        }, function (err, result) {
            if(err){
                return console.error(err);
            }
            console.log('inserted: ');
            console.log(result);
            db.close();
        })
        res.format({
            html: function () {
                res.location("computer"),
                res.redirect("/")
            },
            json: function () {
                res.json(req.body)
            }
        })
    })
})



module.exports = router;
