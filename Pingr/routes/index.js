var express = require('express');
var router = express.Router();

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');
var url = 'mongodb://mongo:27017/pingr';

router.get('/', function(req, res, next) {
    MongoClient.connect(url, function (err,db) {        
                    db.collection('computers').find().toArray( function (err, docs) {
                        if (err) {
                            db.close();
                            return console.error(err);
                        } else {
                            res.render('index', { title: 'Express', "computers": docs, "statusGood": getTrueIfAllPingsAreTrue(docs) });
                            db.close();
                        }
                    })
    });
});


function getTrueIfAllPingsAreTrue(docs) {
    var returnValue = true;
    
    docs.forEach(function(doc) {
        if(doc.pings == null)
            return false;
        if(!doc.pings[doc.pings.length - 1].status)
            returnValue = doc.pings[doc.pings.length - 1].status            
    }, this);
    return returnValue;
}

module.exports = router;
