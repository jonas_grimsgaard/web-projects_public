var ping = require ("net-ping");
var CronJob = require('cron').CronJob;

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');
var ObjectID = require('mongodb').ObjectId;
var url = 'mongodb://mongo:27017/pingr';
    //0 */10 * * * * hvert tiende minutt
    var job = new CronJob("0 */10 * * * *", function(){
         MongoClient.connect(url, function (err,db) {
            assert.equal(null,err);
                findComputers(db, pingHost, function () {
                    db.close();
                })
        })
    });
        
job.start();

function pingHost(ip, session) {    
    session.pingHost (ip, function (error, target) {
        if (error){
            if (error instanceof ping.RequestTimedOutError)
                console.log (target + ": Not alive");
            else
                console.log (target + ": " + error.toString ());
            savedb(ip, false, error.toString());
        }
        else{
            console.log (target + ": Alive");
            savedb(ip, true, null);                        
        }
    });  
}

function findComputers(db, action, callback) {
        var session = ping.createSession ({packetSize: 64});
        var computer = db.collection('computers').find();
        
        computer.each(function (err, doc) {
            assert.equal(err, null);
            if(doc != null){
                action(doc.ip, session);
            }
            else{
                callback();
            }
        })  
}

function savedb(_ip, _status, _message){
    MongoClient.connect(url, function (err, db) {
        if(err != null){
            console.log('Error logging to db: ' + err)
        }
        
        assert.equal(null, err);
        
        console.log("Connected correctly to server");
        
        var computer = db.collection('computers');
        
        computer.findOne({ip: _ip}, function (err, doc) {
            if(doc == null){
               return console.error(err); 
            }
            
            if(_message == null){
                computer.update({_id: new ObjectID(doc._id)}, 
                { 
                    $push: { 
                        pings: {
                            date: new Date(),
                            status: _status
                        } 
                    } 
                }, function (err, count) {
                    if(err){
                        return console.error(err);
                    }
                    return db.close(); 
            }) 
            }            
            else{
                computer.update({_id: new ObjectID(doc._id)}, 
                    { 
                        $push: { 
                            pings: {
                                date: new Date(),
                                status: _status,
                                message: _message
                            } 
                        } 
                    }, function (err, count) {
                        if(err)
                            return console.error(err);
                        return db.close(); 
                }) 
            }
        })
    })
}