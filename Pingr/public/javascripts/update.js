// $('#computerTable .dnstable a').editable({
//     type: 'text',
//     name: 'dnsname',
//     url: '/computer/update',
//     title: 'Enter new dns name',
//     success: function(response, newValue) {
//         console.log(response);
//         if(response.status == 'error') return response.msg; //msg will be shown in editable form
//         else console.log("her ja");
//     }
// });

$('#computerTable').editableTableWidget();

$('table td').on('change', function(evt, newValue) {
	// do something with the new cell value 
    var _ip = $( evt.currentTarget ).attr('data-pk');
    
    $.ajax({
        url: "/computer/update" + '?' + $.param({'ip': _ip, 'newName':newValue}),
        type: 'POST',
        success: function (result) {
            if(result.error)
                return false;
            else
                return true;
        }
    })
});

// $('.dnstable').keypress(function(e) {
//     if(e.which == 13) {
//         alert('You pressed enter!');
//     }
// });

// document.getElementByClassName("dnstable").addEventListener("input", function() {
//     alert("input event fired");
// }, false);