$('.glyphicon-trash').click(function () {
    var currentElement = $( this ).parent().parent();
    var _ip = currentElement.find(".iptable").text();
    
    console.log("IP address: " + _ip);
    console.log("URL ENCODED: " + "/computer" + '?' + $.param({'ip': _ip}))
    
    $.ajax({
        url: "/computer" + '?' + $.param({'ip': _ip}),
        type: 'DELETE',
        success: function (result) {
            console.log("Deleted: " + result);
            currentElement.slideUp();
        }
    }) 
})

$('.glyphicon-trash').hover(function () {
    $(this).parent().parent().addClass('danger');
}, function () {
    $(this).parent().parent().removeClass('danger');
})

var options = {
    'animation' : true,
    'trigger' : 'hover',
    'placement' : 'top'
}

$('.popovers').popover(options)

$('.date').each(function () {
    var date = new Date($(this).text());
    $(this).text(date);
    $(this).parent().find('.glyphicon-ok-circle').attr({'data-toggle':'popover', 'data-content': 'Online since: ' + date.toLocaleString()})
    $(this).parent().find('.glyphicon-exclamation-sign').attr({'data-toggle':'popover', 'data-content': 'Offline since: ' + date.toLocaleString()})
    if($(this).attr('id') != 'keep')
        $(this).remove();
})

$( ".update" ).on( "click", function( e ) {
    var $icon = $( this ).find('.glyphicon-refresh'),
        animateClass = "glyphicon-refresh-animate";

    $icon.addClass( animateClass );
    
    var currentElement = $( this ).parent().parent();
    var _ip = currentElement.find(".iptable").text();
    
    $.ajax({
        url: "/ping" + "?" + $.param({'ip':_ip}),
        type: 'GET',
        success: function (result) {
            console.log("Repinged " + result);
            $icon.attr({'data-toggle':'popover', 'data-content': result});
            $icon.popover(options);
            $icon.removeClass( animateClass );
        },
        error: function (err) {
            console.log("Error from server " + err);
            $icon.removeClass( animateClass );
        }
    })
});    